package bap.fi2011.donutdefense;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.color.Color;

import android.graphics.Typeface;

public class MainActivity extends SimpleBaseGameActivity {

	// ===========================================================
	// Constants
	// ===========================================================
	
	static final int CAMERA_WIDTH = 800;
	static final int CAMERA_HEIGHT = 480;

	// ===========================================================
	// Fields
	// ===========================================================
	
	public Font mFont;
	public Camera mCamera;

	// A reference to the current scene
	public Scene mCurrentScene;
	private BitmapTextureAtlas mBitmapTextureAtlas;
	public ITextureRegion mTestTexture;
	public ITextureRegion mBoxFaceTextureRegion;
	public ITextureRegion mTestBackgroundTexture;
	public ITextureRegion secondTexture;
	private BitmapTextureAtlas mBitmapTextureAtlas2;
	public static MainActivity instance;

	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	public EngineOptions onCreateEngineOptions() {
		instance = this;
		mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR,
				new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), mCamera);
	}

	@Override
	protected void onCreateResources() {
		// TODO Auto-generated method stub
		mFont = FontFactory.create(this.getFontManager(),
				this.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32);
		mFont.load();
		
		
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("");
		/* Load all the textures this game needs. */
		this.mBitmapTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 64, 64);
		
		//this.mTestTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, this, "B.png", 0, 0);
		this.secondTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, this, "Towerbase.png", 0, 0);
		
		
		this.mBitmapTextureAtlas2 = new BitmapTextureAtlas(this.getTextureManager(), 1280, 720);
		this.mTestBackgroundTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas2, this, "testmapImage.png",0,0);
		
		
		
		

		this.mBitmapTextureAtlas2.load();
		this.mBitmapTextureAtlas.load();
	}

	@Override
	protected Scene onCreateScene() {
		// TODO Auto-generated method stub
		mCurrentScene = new TestingclassScene();
		return mCurrentScene;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	// to change the current main scene
	public void setCurrentScene(Scene scene) {
		mCurrentScene = null;
		mCurrentScene = scene;
		getEngine().setScene(mCurrentScene);

	}

	public Color ColorfromRGB(int R, int G, int B) {
		return new Color((float) R / 255, (float) G / 255, (float) B / 255);
	}

	public static MainActivity getSharedInstance() {
		// TODO Auto-generated method stub
		return instance;
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
