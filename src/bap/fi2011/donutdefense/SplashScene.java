package bap.fi2011.donutdefense;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.modifier.IModifier;

public class SplashScene extends Scene {
	MainActivity activity;

	public SplashScene() {
		setBackground(new Background(0.09804f, 0.6274f, 0));
		
		String title_1 = "Donut";
		String title_2 = "Defense";
		
		activity = MainActivity.getSharedInstance();
		
		Text title1 = new Text(0, 0, activity.mFont,
				title_1,
				activity.getVertexBufferObjectManager());
		Text title2 = new Text(0, 0, activity.mFont,
				title_2,
				activity.getVertexBufferObjectManager());

		title1.setPosition(activity.mCamera.getWidth() / 2 -title1.getWidth()/ 2 , -title1.getHeight());
		title2.setPosition(activity.mCamera.getWidth() / 2 -title2.getWidth()/ 2,	activity.mCamera.getHeight() + title2.getHeight());

		attachChild(title1);
		attachChild(title2);

		title1.registerEntityModifier(new MoveYModifier(1, title1.getY(),
				activity.mCamera.getHeight() / 2 - title1.getHeight()));
		title2.registerEntityModifier(new MoveYModifier(1, title2.getY(),
				activity.mCamera.getHeight() / 2));

		loadResources();
	}

	void loadResources() {
		DelayModifier dMod = new DelayModifier(2,
				new IEntityModifierListener() {

					public void onModifierStarted(IModifier<IEntity> arg0,
							IEntity arg1) {
						// TODO Auto-generated method stub
					}

					public void onModifierFinished(IModifier<IEntity> arg0,
							IEntity arg1) {
						activity.setCurrentScene(new MainMenuScene());
					}
				});

		registerEntityModifier(dMod);
	}

}
	