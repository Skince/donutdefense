package bap.fi2011.donutdefense;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.andengine.entity.scene.background.IBackground;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXLoader.ITMXTilePropertiesListener;
import org.andengine.extension.tmx.TMXObject;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.extension.tmx.TMXTileProperty;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.debug.Debug;

import android.R.integer;
import android.R.string;
import android.graphics.Point;

public class Map{

    // ===========================================================
    // Constants
    // ===========================================================
	
    // ===========================================================
    // Fields
    // ===========================================================

	Point Start;
	Point End;
	Point[] Route;
	ITextureRegion background;
	private TMXTiledMap mTMXTiledMap;
	private TMXLoader tmxLoader;
	MainActivity activity;
    // ===========================================================
    // Constructors
    // ===========================================================

//	public Map(MainActivity activity) {
//		this.tmxLoader = new TMXLoader(activity.getAssets(), activity.getTextureManager(), TextureOptions.DEFAULT, activity.getVertexBufferObjectManager(), new ITMXTilePropertiesListener() {
//			
//			public void onTMXTileWithPropertiesCreated(final TMXTiledMap pTMXTiledMap, final TMXLayer pTMXLayer, final TMXTile pTMXTile, final TMXProperties<TMXTileProperty> pTMXTileProperties) {
//				super()
//			}
//		});
//	}
	public Map(ITextureRegion Background,String mapfile) throws TMXLoadException{
		this.background = Background;
		this.activity = MainActivity.getSharedInstance();
		this.tmxLoader = new TMXLoader(activity.getAssets(), activity.getTextureManager(), activity.getVertexBufferObjectManager());
		this.mTMXTiledMap = tmxLoader.loadFromAsset(mapfile);
		this.Route = parseMap(mTMXTiledMap);
	}
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    
    // ===========================================================
    // Methods
    // ===========================================================
	
    private Point[] parseMap(TMXTiledMap mTMXTiledMap2) {
		// TODO Auto-generated method stub
    	ArrayList<TMXObject> objects = mTMXTiledMap2.getTMXObjectGroups().get(0).getTMXObjects();
    	TMXLayer maplayer = mTMXTiledMap2.getTMXLayers().get(0);
    	int[] routeTileIDS = new int[16];
    	for (TMXObject Tile : objects) {
    		String name = Tile.getName();
    		Debug.e(name + " == " + "route"+ " => " + (name == "route"));
			//if( name == "route"){ totally bugs and returns false even if "route" == "route"
    		if ( name.hashCode() == "route".hashCode()){
				int TileX = Tile.getX() /mTMXTiledMap2.getTileWidth();
				int TileY = Tile.getY() /mTMXTiledMap2.getTileHeight();
				
				int TilewidthX = Tile.getWidth()/mTMXTiledMap2.getTileWidth() + TileX;
				int TileheightY = Tile.getHeight() / mTMXTiledMap2.getTileHeight() + TileY;
				int i = 0;
				for (int x = TileX; x < TilewidthX; x++) {
					for (int y = TileY; y < TileheightY; y++) {
						routeTileIDS[i] = maplayer.getTMXTile(x, y).getGlobalTileID();
						i++;
					}
				}
				break;
			}
		}
    	// OK now i have every Tile ID which is supposed to be a part of the Route in routeTileIDS
    	// time to get the turning points of the route
		return null;
	}

	public void SetMap(String Map) throws TMXLoadException {
    }
    
    public boolean isValidMove(int x,int y){
    	for (Point point : Route) {
			if (point.equals(x, y)) {
				return true;
			}
		}
    	return false;
    	
    }
    public TMXLayer getLayer(int i) throws Exception{
    	throw new Exception("NOT YET IMPLEMENTED");
    	//return this.mTMXTiledMap.getTMXLayers().get(i);
    }

	public IBackground getSpriteBackground() {
		Sprite Background = new Sprite(0, 0, background, activity.getVertexBufferObjectManager());
		Background.setSize(MainActivity.CAMERA_WIDTH, MainActivity.CAMERA_HEIGHT);
		
		return new SpriteBackground(Background);
	}
    
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    
}
